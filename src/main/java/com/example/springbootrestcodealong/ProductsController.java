package com.example.springbootrestcodealong;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products")
@AllArgsConstructor
public class ProductsController {

    ProductService productService;

    @GetMapping
    public List<Product> all() {
        return productService.all()
                .map(ProductsController::toDTO)
                .collect(Collectors.toList());
    }

    @PostMapping
    public Product create(@RequestBody CreateProduct createProduct) {
        ProductEntity productEntity = productService.create(createProduct.getName(), createProduct.getPrice());
        return toDTO(productEntity);
    }

    @GetMapping("/{productId}")
    public Product get(@PathVariable("productId") String productId) {
        return new Product(
                productId,
                "Watch",
                "The best watch ever",
                "100 SEK");
    }

    @PutMapping("/{productId}")
    public Product put(
            @PathVariable("productId") String productId,
            @RequestBody UpdateProduct updateProduct) {
        return new Product(
                productId,
                updateProduct.getName(),
                updateProduct.getDescription(),
                "100 SEK");
    }

    @DeleteMapping("/{productId}")
    public Product delete(@PathVariable("productId") String productId) {
        return new Product(
                productId,
                "Watch",
                "The best watch ever",
                "100 SEK");
    }


    private static Product toDTO(ProductEntity productEntity) {
        return new Product(
                productEntity.getId(),
                productEntity.getName(),
                productEntity.getDescription(),
                ""+ productEntity.getPrice()+" SEK"
        );
    }

}
