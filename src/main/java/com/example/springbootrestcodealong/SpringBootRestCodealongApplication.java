package com.example.springbootrestcodealong;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestCodealongApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestCodealongApplication.class, args);
    }

}
