package com.example.springbootrestcodealong;

import lombok.Value;

@Value
public class Product {
    String id;
    String name;
    String description;
    String price;
}
