package com.example.springbootrestcodealong;

import lombok.Value;

@Value
public class CreateProduct {
    String name;
    String price;
}
