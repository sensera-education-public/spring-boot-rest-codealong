package com.example.springbootrestcodealong;

import lombok.Data;

import java.util.UUID;

@Data
public class ProductEntity {
    String id;
    String name;
    String description;
    double price;

    public ProductEntity(String name, String description, double price) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
        this.price = price;
    }
}
