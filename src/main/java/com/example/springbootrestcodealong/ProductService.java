package com.example.springbootrestcodealong;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class ProductService {
    public Stream<ProductEntity> all() {
        return Stream.of(
                new ProductEntity("Watch","The best watch ever", 100)
        );
    }

    public ProductEntity create(String name, String price) {
        return new ProductEntity(
                name,
                "",
                Double.parseDouble(price)
        );
    }
}
