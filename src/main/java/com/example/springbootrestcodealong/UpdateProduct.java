package com.example.springbootrestcodealong;

import lombok.Value;

@Value
public class UpdateProduct {
    String name;
    String description;
}
